# Prpg ClubInfp
> YAL IMA GIS, TOUS UNIS (sauf les YAL).

## A installer
Nous utilisons C++ ainsi que la librairie SFML

[Guide d'installation](https://docs.google.com/document/d/1IxzuAC4azZ7HbOWgk6kKnj8sO4M_UEQX8hLQcDjdJ-4/edit?usp=sharing)


## [Conventions des commits](https://www.conventionalcommits.org/en/v1.0.0-beta.4/)

Le commit doit être structuré comme suit:
```
  <type>([optional scope]): <description>
	  [optional body]

	  [optional footer]
```
--------
1. **Type**: définit le type de commit
    -   **build**: Système de build (example : gulp, webpack, npm)
    -   **ci**: Intégration continue (example scopes: Travis, Circle, BrowserStack, SauceLabs)
    -   **docs**: Documentation
    -   **feat**: Ajout d'une fonctionnalité
    -   **fix**: Correction de bogue
    -   **perf**: Amélioration des performances
    -   **refactor**: Changement du code qui ne change rien au fonctionnement
    -   **style**: Changement du style du code (sans changer la logique)
    -   **test**: Modification des tests
2.  **Scope**: définit quelle partie de votre librairie / application est affectée par le commit (cette information est optionnelle)
3.  **Sujet**:  contient une description succinte des changements
    -   En utilisant l'impératif présent ("change", et non pas "changed" ou "changes")
    -   Sans majuscule au début
    -   Pas de "." à la fin de la description
4.  **Description**: permet de détailler plus en profondeur les motivations derrière le changement. Les règles sont les mêmes que pour la partie Sujet.
5.  **Footer**: contient les changements importants (Breaking Changes) et les références à des issues [GitHub](https://help.github.com/articles/closing-issues-using-keywords/) / GitLab ou autre.

## [Cahier des charges](https://docs.google.com/spreadsheets/d/1LfVxpDZ2HGnKapykHs5gPgjRBHqGzPFiUee5d-tNeII/edit?usp=sharing)

## [Diagramme de classes](https://drive.google.com/file/d/1WT9R-smmqTTezJWtnXfc_BrPnXnskQYK/view?usp=sharing)

## Outils création assets

Graphisme : 
	Inkscape pour création de logo
	Paint pour pixelart-making
	https://www.imgonline.com.ua/eng/make-seamless-texture.php Jsp si fonctionne bien
	Gimp, Pixlr.com pour colorimétrie
	http://piskelapp.com Online pixel-art et possible animation

Sons :
	Beepbox.co pour la création de musique, accessible à tous
	Audacity : utile pour tout le reste

Story writing :
	https://www.nomsdefantasy.com/ Générateur de noms

Déploiement : 
	Android https://fr.sfml-dev.org/forums/index.php?topic=15337.0

Drive : 
    https://drive.google.com/drive/folders/1phEz04cjJ1dmIOwOPY_giANOWqHqut1q
     Contient les fichiers multimédias ainsi que les documents liés ci-dessus.



